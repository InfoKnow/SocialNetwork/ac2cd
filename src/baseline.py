import igraph as ig

from itertools import accumulate

def subordinating_strength(v, C, G):
    neis = G.neighbors(v)
    neis_in_C = C.intersection(neis)

    num = accumulate([G[v,u] for u in neis_in_C])
    den = accumulate([G[v,u] for u in neis])

    return num/den



#=============================================
# Update strategies
#=============================================

def update_communities_mixed_type(v_old, v_new, ctk, sub_g, G):
    '''
    There are new and old nodes
    '''
    update_CS_t1 = 0
    for v in v_old:
        s_ctk_v = subordinating_strength(v, ctk, G)
        s_sub_g_v = subordinating_strength(v, sub_g, G)

        if s_ctk_v <= s_sub_g_v:
            sub_g.add(v)
        else:
            sub_g.remove(v)
            ctk.add(v)
    for v in v_new:
        s_ctk_v = subordinating_strength(v, ctk, G)
        s_sub_g_v = subordinating_strength(v, sub_g, G)
        if s_ctk_v <= s_sub_g_v:
            sub_g.add(v)
        else:
            sub_g.remove(v)
            ctk.add(v)
    ## What is update_CS_t1 ???
    return update_CS_t1.union(sub_g.vs).union(ctk.vs)

def update_communities_multi_cont(list_C, sub_g, G):
    '''
    All vertices are old and distributed in several communities
    '''
    update_CS_t1 = 0
    for k in len(list_C):
        vs_k = set(list_C[k].vs).intersect(sub_g)
        for v in vs_k:
            s_ctk_v = subordinating_strength(v, list_C[k], G)
            s_sub_g_v = subordinating_strength(v, sub_g, G)
            # Deveria fazer alguma coisa com essas variaveis
    for v in [c.vs for c in list_C]:
        if max(s_ctk_v) <= s_sub_g_v:
            sub_g.add(v)
        else:
            sub_g.remove(v)
            # add to the community with maximum strength



if __name__ == "__main__":
    pass
