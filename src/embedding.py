from torch.optim import SparseAdam
from torch_geometric.nn import Node2Vec
from torch import no_grad

class Encoder():
    def __init__(self, data, device='cpu'):
        self.data = data
        self.device = device
        edge_index = data.edge_index
        self.model = Node2Vec(edge_index, embedding_dim=200, walk_length=20,
             context_size=10, walks_per_node=10,
             num_negative_samples=1, p=1, q=1, sparse=True).to(self.device)

        self.loader = self.model.loader(batch_size=256, shuffle=True, num_workers=4)
        self.optimizer = SparseAdam(list(self.model.parameters()), lr=0.01)

    def _train(self):
        self.model.train()
        total_loss = 0
        for pos_rw, neg_rw in self.loader:
            self.optimizer.zero_grad()
            loss = self.model.loss(pos_rw.to(self.device), neg_rw.to(self.device))
            loss.backward()
            self.optimizer.step()
            total_loss += loss.item()
        return total_loss / len(self.loader)


    @no_grad()
    def test(self):
        self.model.eval()
        z = self.model()
        acc = self.model.test(z[self.data.train_mask], self.data.y[self.data.train_mask],
                        z[self.data.test_mask], self.data.y[self.data.test_mask],
                        max_iter=10)
        return acc
    
    def get_trained_model(self):
        for epoch in range(1, 101):
            loss = self._train()
            #acc = self.test()
            if epoch % 10 == 0:
                print(f'emb-Epoch: {epoch:02d}, Loss: {loss:.4f}')
        return self.model()
