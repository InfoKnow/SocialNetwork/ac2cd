import gym
#import tensorflow as tf
import numpy as np
from stable_baselines3 import PPO
import os
import environment
import dataset
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.logger import configure
from stable_baselines3.common.monitor import Monitor

new_logger = configure('./ppo_tensorboard', ['stdout', 'tensorboard'])


class TensorboardCallback(BaseCallback):
    def __init__(self, verbose=0):
        super(TensorboardCallback, self).__init__(verbose)

    def _on_step(self) -> bool:
        value = np.random.random()
        self.logger.record('random_value', value)
        
        return True

models_dir = '/tmp/rl/models/PPO'
logdir = '/tmp/rl/log'
model_path='./sb3_model.zip'
if not os.path.exists(models_dir):
    os.makedirs(models_dir)
if not os.path.exists(logdir):
    os.makedirs(logdir)
debug: bool = True

#env = gym.make("LunarLander-v2")
#DS = dataset.SYNDataset(device='cpu')
DS = dataset.Email_Eu_core(device='cuda')

env = gym.make('gatenv-v0', dataset=DS)
monitor = Monitor(env)

if os.path.exists(model_path):
    print('Loading model', end=' ')
    model = PPO.load(model_path)
    print('done !')
else:
    model = PPO("MlpPolicy", monitor, verbose=2, tensorboard_log='ppo_tensorboard')

    model.set_logger(new_logger)

    print("Learning ...")
    model.learn(
        total_timesteps=10000,
        #callback=TensorboardCallback()
    )
    print("Model trained \U0001f600")

    print("Saving ...")
    PPO.save(model, model_path)

episodes = 100
for step in range(episodes):
    obs = env.reset()
    done = False
    while not done:
        env.render()
        #obs, reward, done, info = env.step(env.action_space.sample())
        action, _ = model.predict(obs)
        obs, reward, done, info = env.step(action)
        # if debug is True:            
        #     print(step,'Action:', action,
        #         'Observation:', obs,
        #         'Reward:', reward,
        #         'Done', done,
        #         'Info:', info)

    print(reward)

env.close()