import torch
from torch.optim import Adam
import torch.nn.functional as F
import torch.nn as nn
import torch_geometric.transforms as T
from torch_geometric.nn import GATConv, GCNConv
from sklearn.metrics import normalized_mutual_info_score
import warnings
import numpy as np

class GCNNet(torch.nn.Module):
    def __init__(self, in_channels, out_channels, dropout=0.6, critic=False):
        super(GCNNet, self).__init__()

        self.critic = critic

        self.conv1 = GCNConv(in_channels, 16)
        self.conv2 = GCNConv(16, out_channels)
        
    def forward(self, x, edge_index):
        x = F.dropout(x, p=0.6, training=self.training)
        x = F.leaky_relu(self.conv1(x, edge_index))
        x = F.dropout(x, p=0.6, training=self.training)
        if self.critic is True:
            return self.conv2(x, edge_index.t())
        else:
            x = F.leaky_relu(self.conv2(x, edge_index.t()))
            return F.log_softmax(x, dim=-1)

class GATNet(torch.nn.Module):
    def __init__(self, in_channels, out_channels, dropout=0.6, critic=False):
        super(GATNet, self).__init__()

        self.critic = critic

        # Baseado na implementação fo pytorch_geometric
        # https://github.com/pyg-team/pytorch_geometric/blob/master/examples/gat.py

        self.conv1 = GATConv(in_channels, 16, heads=1, dropout=dropout)
        #self.conv2 = GATConv(8 * 8, out_channels, heads=1, concat=False, dropout=0.6)
        #self.conv2 = GATConv(16, 64, heads=1, dropout=0.8)
        self.conv2 = GATConv(16, out_channels, heads=1, concat=False, dropout=dropout, add_self_loops=False)
        self.previous_out = torch.zeros(in_channels, out_channels)


    def forward(self, x, edge_index):
        x = F.dropout(x, p=0.6, training=self.training)
        x = F.leaky_relu(self.conv1(x, edge_index))
        x = F.dropout(x, p=0.6, training=self.training)
        if self.critic is True:
            return self.conv2(x, edge_index)
        else:
            conv_res = self.conv2(x, edge_index)
            x = F.leaky_relu(conv_res, inplace=True)
            return F.log_softmax(x, dim=-1)


def get_model(num_features: int, classes: int, device: str, nn_type : str ='gat') -> torch.nn.Module:
    print(f'Creating {nn_type} model')
    if nn_type == 'gat':
        return GATNet(num_features, classes).to(device)
    else:
        return GCNNet(num_features, classes).to(device)


def train_model(model: torch.nn.Module, data, optimizer: Adam):
    model.train()
    optimizer.zero_grad()
    
    out = model(data.x, data.edge_index)    
    
    loss = F.nll_loss(out[data.train_mask], data.y[data.train_mask])
    #loss = F.nll_loss(model()[data.train_mask], data.y[data.train_mask])
    #FIXME: Consertar os parametros da função
    #loss = my_composed_loss(out[data.train_mask], data.y[data.train_mask], model.previous_out[data.train_mask].cuda())
    #print(f"Loss: {loss}")
    loss.backward()#retain_graph=True)
    optimizer.step()
    model.previous_out = out.detach()
    return loss.item()


@torch.no_grad()
def test(model, data):
    model.eval()
    out = model(data.x, data.edge_index)
    accs = []
    for mask in [data.train_mask, data.test_mask]:
        acc = float((out[mask].argmax(-1) == data.y[mask]).sum() / mask.sum())
        accs.append(acc)

    out_cpu = torch.argmax(out.detach().cpu(), dim=-1)
    y_cpu = data.y.detach().cpu()
    nmi = normalized_mutual_info_score(y_cpu, out_cpu, average_method='arithmetic')
    accs.append(nmi)

    return accs


def save_checkpoint(model, optimizer, path, epoch):
    torch.save({
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'epoch': epoch
    }, path)

def load_checkpoint(model, optimizer, path):
    checkpoint = torch.load(path)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']

    return model, optimizer, epoch

def loss_similarity(s1: set, s2: set):
    '''Computes the difference between two representations'''
    return 1 / len(s2.intersection(s1))

def loss_community_assignment(o, h):
    '''Computes the loss of community attribuition'''
    return  

def loss_time_smoothness(h_t, h_t_1):
    '''
    Computes the loss of two sequential community attribuition using the 
    euclidean distance between them
    '''
    #return np.linalg.norm(h_t - h_t_1)
    return h_t - h_t_1

def my_composed_loss(output, target, previous, gamma=.5):
    return F.cross_entropy(output, target, reduction='mean')

    #return ((gamma * F.cross_entropy(output, target, reduction='mean')) + \
    #        ((1-gamma) * loss_time_smoothness(output, previous))).mean()
