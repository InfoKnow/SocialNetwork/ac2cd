import gym
from gym import spaces
import logging
import numpy as np
import torch
from typing import List, Tuple, Union
from numpy.typing import ArrayLike

from gym.envs.registration import register

from sklearn.metrics import f1_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics.cluster import normalized_mutual_info_score
from utils import PerformancePlotter

register(id='gatenv-v0', entry_point='environment:GATEnv')

class GATEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, dataset):
        super(GATEnv, self).__init__()

        self.render_mode='human'

        self.dataset=dataset
        data=self.dataset[0]
        self.num_nodes=data.x.shape[0]
        self.dimensions=data.x.shape[1]
        self.current_node = 0
        self.last_modularity=0
        self.visualization=None
        self.n_actions=200

        logging.info("Loading environment with shape %s", data.x.shape)

        # Para comunidades sobrepostas precisamos alterar a dimensão da matriz
        #communities = np.zeros((self.num_nodes, self.dimensions), int)
        communities = np.zeros((self.n_actions,), int)
        #np.fill_diagonal(communities, 1)

        # Cada ação representa um vetor e cada elemento a comunidade que deve ser associada ao nó daquela posição
        self.action_space = spaces.Box(0, 1, shape=(self.n_actions,), dtype=int) 

        # A matrix de associação entre nós e comunidades
        #self.observation_space = spaces.Box(-2, self.num_nodes-1, shape=(self.num_nodes,self.num_nodes), dtype=np.float32) 
        self.observation_space = spaces.Box(-2, self.num_comm-1, shape=(self.num_nodes,self.dimensions), dtype=np.float32) 

        self.state = communities

        #metrics
        self.f1_micro = list()
        self.f1_macro = list()
        

    def step(self, action: ArrayLike):
        '''
        @description: Apply the communities to each node
        @param action: A list of node assignments at each position a community id
        '''
        self.current_node += 1
                    
        #TODO: Criar o componente para diminuir o "drift" entre duas ações consecutivas        
        edge_index = self.dataset[0].edge_index

        norm_action = np.array(action) % self.dimensions 
        communities = norm_action.astype(int).tolist()

        #modularity = GATEnv.mod_density(communities, edge_index)
        labels_true = np.array(self.dataset[0].y) #.reshape(-1,1)
        labels_pred = norm_action #.reshape(-1,1)
        modularity = euclidean_distances(
                labels_pred.reshape(-1,1),
                labels_true.reshape(-1,1)
            ).sum()
        nmi = normalized_mutual_info_score(
            labels_true[:len(labels_pred)],
            labels_pred
        )

        #print('!!! MNI:', nmi)

        # self.f1_micro.append(
        #     f1_score(self.dataset[0].y, communities, average='micro')
        # )
        # self.f1_macro.append(
        #     f1_score(self.dataset[0].y, communities, average='macro')
        # )
        # print('Modularity', modularity,
        #     '\nCommunities', communities,
        #     'Ground-Thuth', self.dataset[0].y,
        # "\nf1_micro:", f1_micro,
        # '\nf1_macro', f1_macro
        # )
        #if self.last_modularity < modularity:
        if self.last_modularity < nmi:
            reward = 1
            for node in range(len(communities)):
                # action[node]: node's community
                self.state[node] = torch.tensor(communities[node], dtype=int)
        else:
            reward = -1
        #self.last_modularity = modularity
        self.last_modularity = nmi

        done = True if self.current_node == (self.num_nodes-1) else False        
        #return self.state, reward, done, {}
        return self.dataset[0].x.detach(), reward, done, {}

    def reset(self):
        self.state = np.zeros((self.num_nodes, 1), int)        
        self.current_node = 0
        self.last_modularity = 0
        return self.dataset[0].x.detach()
        self.f1_macro = list()
        self.f1_micro = list()

    def render(self, mode='human'):
        assert mode in ['human', 'rgb_array'], "Invalid mode, must be 'human' or 'rgb_array'"
        
        # if mode == 'human':
        #     if self.visualization == None:
        #         self.visualization = PerformancePlotter()
        #     else:
        #         self.visualization.render(self.current_node, self.f1_macro, self.f1_micro)

    def close(self):
        pass

    def edge_index(self):
        return self.dataset[0].edge_index


    @staticmethod
    def mod_density(communities: torch.Tensor, edge_index: torch.Tensor):
        '''
        @param communities: Uma lista de conjuntos, cada conjunto é uma comunicade e seus 
        elementos são os ids dos nós
        '''
        import networkx as nx
        from cdlib import evaluation
        from cdlib import NodeClustering

        g = nx.Graph()
        shape = edge_index.shape
        edge_list=edge_index.tolist()
        g.add_edges_from(edge_list)

        comm_by_node = list()
        for node_id,comm_id in enumerate(communities):
            if comm_id >= 0: # Guard for invalid community id
                try:
                    comm_by_node[comm_id].append(node_id)
                except IndexError as err:
                    comm_by_node.insert(comm_id, [node_id])

        surrogate = NodeClustering(communities=comm_by_node, graph=g, overlap=False)
        return evaluation.modularity_density(g, surrogate).score

    @staticmethod
    def modularity_density(communities: torch.Tensor, edge_index: List[Tuple[int,int]]):
        #FIXME: Esse método está demorando muito para executar
        m = len(edge_index)
        Qds = 0

        for comm_idx in range(communities.shape[0]):
            n_c = len([n for n in communities[comm_idx] if n != 0])
            #n_c = torch.sum(communities[comm_idx])
            
            m_c = len([edge for edge in edge_index if edge[0] in communities[comm_idx] and edge[1] in communities[comm_idx]])
            
            p_c = (2 * m_c) / (n_c * (n_c-1))

            c_e = 0
            for edge in edge_index:
                if edge[0] in communities[comm_idx] and edge[1] not in communities[comm_idx]:
                    c_e += 1
                elif edge[1] in communities[comm_idx] and edge[0] not in communities[comm_idx]:
                    c_e += 1

            #TODO: validar essas atribuições
            sum_m_cc = c_e
            sum_p_cc = sum_m_cc/(n_c * communities[0].shape[0])

            first_term = (m_c/m)*p_c
            split_penalty = (sum_m_cc * sum_p_cc) / (2 * m)
            intermediate_term = (p_c * ((2 * m_c + c_e)/(2*m)))** 2
            
            Qdsc = first_term - intermediate_term - split_penalty
            Qds += Qdsc

        return Qds
        