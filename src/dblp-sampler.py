import datetime
import gzip
import time
import os
import pickle
from xml.sax import ContentHandler, make_parser
from xml.sax.handler import feature_validation, feature_external_ges
import json
import logging
from logging.handlers import RotatingFileHandler
from itertools import permutations

logging.basicConfig(
    format='%(name)s - %(levelname)s - %(message)s',
    handlers=[RotatingFileHandler('dblpsampler.log', maxBytes=10000000, backupCount=5)],
    level=logging.INFO)
logger = logging.getLogger("DBLPSampler log")

dblpxml = "dblp.xml.gz"
nodes = set()
edges = []

publication_counter = 0

def savePub(publication):
    global publication_counter
    publication_counter = publication_counter + 1
    authors = publication['author']
    nodes.update(authors)
    year = publication['year']
    for pair in permutations(authors,2):
        edges.append((pair[0], pair[1], year))


class DBLPContentHandler(ContentHandler):
    inPublication = True
    currentPubName = ''
    attrs = {}
    value = ''

    def __init__(self):
        
        super()
        self.allowedtypes = [
            #'www',
            'inproceedings',
            'proceedings',
            #'book',
            'incollection',
            #'phdthesis',
            'article',
            #'mastersthesis'
            ]
        self.conferences = ['IJCAI', 'AAAI', 'KDD', 'CIKM', 'ICDE', 'ICDM', 'PAKDD']
        self.timeslice = (1994,2015)

    
    def startDocument(self):
        self.pubAttrs = ''

    def endDocument(self):
        pass

    def startElement(self, name, attrs):
        if name in self.allowedtypes:
                DBLPContentHandler.inPublication = True
                DBLPContentHandler.currentPubName = name
                DBLPContentHandler.attrs['key'] = attrs['key']
                DBLPContentHandler.attrs['furthertype'] = attrs.get('publtype', '')

    def endElement(self, name):
        if DBLPContentHandler.inPublication is True:
            if DBLPContentHandler.currentPubName == name:
                DBLPContentHandler.attrs["type"] = name
                if DBLPContentHandler.attrs['furthertype'] != 'informal' or name != 'www':
                    try:
                        if DBLPContentHandler.attrs["booktitle"] in self.conferences and (self.timeslice[0] <= int(DBLPContentHandler.attrs["year"]) <= self.timeslice[1]):
                            savePub(DBLPContentHandler.attrs)
                    except KeyError as err:
                        logger.error(err)
                    
                # Flush object                
                DBLPContentHandler.inPublication = False
                DBLPContentHandler.attrs = {}
            else:
                if name == "author":
                    if DBLPContentHandler.attrs.get(name) is not None:
                        DBLPContentHandler.attrs[name].append(DBLPContentHandler.value.strip())
                    else:
                        DBLPContentHandler.attrs[name] = [DBLPContentHandler.value]
                else:
                    DBLPContentHandler.attrs[name] = DBLPContentHandler.value
        DBLPContentHandler.value = ''
        
    def characters(self, content):
        if content != '':
            DBLPContentHandler.value += content.replace('\n', '')



    
with gzip.open(dblpxml, mode='rt', encoding='latin-1') as fp:
    parser = make_parser()
    
    start_time = time.time()
    parser.setContentHandler(DBLPContentHandler())
    parser.setFeature(feature_external_ges, True)
    parser.parse(fp)
    
    elapsed = time.time() - start_time
    print("File parser in ", datetime.timedelta(seconds=elapsed))

print(f"{len(nodes)} autores carregados em {publication_counter} publicações")
# The paper claims to use 30489 authors in 20931 papers
#assert len(nodes) == 30489

# Salva o conteudo de nodes e edges
with open('dblp-sample.nodes', 'bw') as file_nodes:
    pickle.dump(nodes, file_nodes)
with open('dblp-sample.edges', 'bw') as file_edges:
    pickle.dump(edges, file_edges)
