import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from time import time
import seaborn as sns

def plot_learning_curve(x, scores, figure_file):
    running_avg = np.zeros(len(scores))
    for i in range(len(running_avg)):
        running_avg[i] = np.mean(scores[max(0, i-100):(i+1)])
    plt.plot(x, running_avg)
    plt.title('Running average of previous 100 scores')
    plt.savefig(figure_file)

def timer_func(func):
    # This function shows the execution time of 
    # the function object passed
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'Function {func.__name__!r} executed in {(t2-t1):.4f}s')
        return result
    return wrap_func

def dict_to_matrix(dic: dict):
    matrix = list()
    for key in dic.keys():
        matrix[key].append(dic[key])

class PerformancePlotter:
    def __init__(self):
        fig = plt.figure()
        fig.suptitle('AC2CD Performance')

        plt.show(block=False)
    def render(self, *args, **kwargs):
        current_step = args[0]
        x = range(current_step)
        macro = args[1]
        micro = args[2]

        plt.clf()

        ax = sns.lineplot(x=x, y=macro, label='F1-Macro')
        ax = sns.lineplot(x=x, y=micro, label='F1-Micro')

        plt.xlabel('Time steps')
        plt.ylabel('Performance')

        plt.pause(0.01)