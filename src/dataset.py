import networkx as nx
import numpy as np
from scipy.sparse import coo_array
import torch
import torch.nn.functional as F
import pandas as pd
import logging
import json
import embedding
from collections import defaultdict


from torch_geometric.utils.convert import from_networkx
from torch_geometric.data import InMemoryDataset, Data
from torch_geometric.nn import Node2Vec

import sys

from itertools import chain
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


#TODO: Create datasets: SYN, SYN-FIX, SYN-VAR e SYN-EVENT
root_logger = logging.getLogger()
root_logger.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root_logger.addHandler(handler)


class High_School(InMemoryDataset):
    def __init__(self, root='high_school_dataset', transform=None, pre_transform=None, device='cpu', config=''):
        super().__init__(root, transform, None, None)
        self.labels = []
        self.name='High_School'
        self.device=device

        self.load()

    @property
    def raw_file_names(self):
        return ['high_school-temporal.txt']
    
    @property
    def processed_file_names(self):
        return ['data.pt']
    
    def download(self):
        pass

    def step(self):
        raise NotImplementedError()

    def loadLabels(self):
        labels = []
        
        def default_label():
            return -1

        labelmap = defaultdict(default_label)
        
        try:
            with open('Datasets/high-school-contact-and-friendship-networks/metadata_2013.txt') as fp:
                for line in fp.readlines():
                    node, label, gender = line.strip().split('\t')
                    if labelmap[label] == -1:
                        labelmap[label] = len(labelmap)
                    idx_label = labelmap[label]
                    labels.append((int(node), idx_label))
        except IOError as err:
            raise Exception("Error loading labels data " + err)
        
        return labels

    def load(self):
        features = 100
        graph = nx.Graph()
        edge_list = list()
        try:
            with open(
                'Datasets/high-school-contact-and-friendship-networks/Friendship-network_data_2013.csv',
                encoding='utf-8'
            ) as fp:
                for line in fp.readlines():
                    source, target = line.split(' ')
                    edge_list.append((int(source), int(target)))
        except IOError as err:
            raise Exception('Error opening edges list: ' + err)

        graph.add_edges_from(edge_list)
        dimension = graph.number_of_nodes()
        print('Nodes:', graph.number_of_nodes())
        print('Edges:', graph.number_of_edges())

        
        # get train test and val sizes: (70% - 15% - 15%)
        train_size = int(len(graph.nodes())*0.7)
        test_size = int(len(graph.nodes())*0.85) - train_size
        val_size = len(graph.nodes()) - train_size - test_size

        labels = self.loadLabels()
        #graph_data = graphToTensor(graph, labels)
        #encoder = embedding.Encoder(data, index, device=self.device)

        data = from_networkx(graph)
        encoder = embedding.Encoder(data.edge_index, device=self.device)

        x=None
        try:
            x = torch.load('highschool_embedding.pt')
            if isinstance(x, Node2Vec):
                x = x()
        except Exception as err:
            emb = encoder.get_trained_model()
            x = emb
        torch.save(x, 'highschool_embedding.pt')
        
        data.x = x
        data.y = [node_comm[1] for node_comm in labels]

        self.data = data

class BlogCatalog(InMemoryDataset):
    def __init__(self, root='blogcatalog_dataset', transform=None, pre_transform=None, device='cpu', config=''):
        super().__init__(root, transform, None, None)
        self.labels = []
        self.name='BlogCatalog'
        self.device=device
        self.load()

    @property
    def raw_file_names(self):
        return ['blogcatalog.txt']
    
    @property
    def processed_file_names(self):
        return ['data.pt']
    
    def download(self):
        pass

    def step(self):
        raise NotImplementedError()

    def loadLabels(self):
        labels = []
        try:
            with open('Datasets/BlogCatalog-dataset/data/group-edges.csv') as fp:
                for line in fp.readlines():
                    node, label = line.strip().split(',')
                    labels.append((int(node), int(label)))
        except IOError as err:
            raise Exception("Error loading labels data " + err)
        
        return labels

    def process(self):
        pass
    def load(self):
        features = 100
        graph = nx.Graph()
        edge_list = list()
        try:
            with open('Datasets/BlogCatalog-dataset/data/edges.csv', encoding='utf-8') as fp:
                for line in fp.readlines():
                    source, target = line.split(',')
                    edge_list.append((int(source), int(target)))
        except IOError as err:
            raise Exception('Error opening edges list' + err)

        graph.add_edges_from(edge_list)
        dimension = graph.number_of_nodes()
        print('Nodes:', graph.number_of_nodes())
        print('Edges:', graph.number_of_edges())

        #adj = nx.to_scipy_sparse_array(graph, format='coo', dtype=int)
        #index = torch.LongTensor(
        #    np.vstack((adj.row, adj.col)),
        #)
        
        #data = Data(edge_index=torch.transpose(index, 0, 1))        
        data = from_networkx(graph)
        
        index = torch.LongTensor(
            np.vstack((adj.row, adj.col))
        )
        
        #data = Data(edge_index=torch.transpose(index, 0, 1))        
        #encoder = embedding.Encoder(data, index, device=self.device)
        # get train test and val sizes: (70% - 15% - 15%)
        nodes = list(graph.nodes())
        train_size = int(len(nodes)*0.7)
        test_size = int(len(nodes)*0.85) - train_size
        val_size = len(nodes) - train_size - test_size

        train_set = nodes[0:train_size]
        test_set = nodes[train_size:train_size+test_size]
        val_set = nodes[train_size+test_size:]

        # build test train val masks
        train_mask = torch.zeros(len(nodes),dtype=torch.long, device=self.device)
        for i in train_set:
            train_mask[i-1] = 1.

        test_mask = torch.zeros(len(nodes),dtype=torch.long, device=self.device)
        for i in test_set:
            test_mask[i-1] = 1.
            
        val_mask = torch.zeros(len(nodes),dtype=torch.long, device=self.device)
        for i in val_set:
            val_mask[i-1] = 1.

        labels = self.loadLabels()
        #graph_data = graphToTensor(graph, labels)
        #encoder = embedding.Encoder(data, index, device=self.device)

        data = from_networkx(graph)
        data.train_mask = train_mask
        data.test_mask = test_mask
        data.val_mask = val_mask
        encoder = embedding.Encoder(data, device=self.device)
        
        x=None
        try:
            x = torch.load('BlogCatalog_embedding.pt')
            if isinstance(x, Node2Vec):
                x = x()
        except Exception as err:
            emb = encoder.get_trained_model()
            x = emb
        torch.save(x, 'BlogCatalog_embedding.pt')
        
        data.x = x()
        
        data.y = [node_comm[1] for node_comm in self.loadLabels()]

        self.data = data
    


class Flickr(InMemoryDataset):
    def __init__(self, root='flickr_dataset', transform=None, pre_transform=None, device='cpu', config=''):
        super().__init__(root, transform, None, None)
        self.labels = []
        self.name='Flickr'
        self.device=device
        self.load()

    @property
    def raw_file_names(self):
        return ['flickr.txt']
    
    @property
    def processed_file_names(self):
        return ['data.pt']
    
    def download(self):
        pass

    def step(self):
        raise NotImplementedError()

    def loadLabels(self):
        labels = []
        try:
            with open('Datasets/Flickr-dataset/data/group-edges.csv') as fp:
                for line in fp.readlines():
                    node, label = line.strip().split(',')
                    labels.append((int(node), int(label)))
        except IOError as err:
            raise Exception("Error loading labels data " + err)
        
        return labels

    def process(self):
        pass
    def load(self):
        features = 100
        graph = nx.Graph()
        edge_list = list()
        try:
            with open('Datasets/Flickr-dataset/data/edges.csv', encoding='utf-8') as fp:
                for line in fp.readlines():
                    source, target = line.split(',')
                    edge_list.append((int(source), int(target)))
        except IOError as err:
            raise Exception('Error opening edges list' + err)

        graph.add_edges_from(edge_list)
        dimension = graph.number_of_nodes()
        print('Nodes:', graph.number_of_nodes())
        print('Edges:', graph.number_of_edges())

        
        data = from_networkx(graph)
        
        encoder = embedding.Encoder(data, device=self.device)

        x=None
        try:
            x = torch.load('Flickr_embedding.pt')
            if isinstance(x, Node2Vec):
                x = x()
        except Exception as err:
            emb = encoder.get_trained_model()
            x = emb
        torch.save(x, 'Flickr_embedding.pt')
        
        data.x = x
        
        data.y = [node_comm[1] for node_comm in self.loadLabels()]

        self.data = data
    

class Email_Eu_core(InMemoryDataset):
    def __init__(self, root='email_eu_core_dataset', transform=None, pre_transform=None, device='cpu', config=''):
        super().__init__(root, transform, None, None)
        self.labels = []
        self.name='Email_Eu_core'
        self.device=device
        self.load()

    @property
    def raw_file_names(self):
        return ['email-Eu-core-temporal.txt']
    
    @property
    def processed_file_names(self):
        return ['data.pt']
    
    def download(self):
        pass

    def step(self):
        raise NotImplementedError()

    def loadLabels(self):
        labels = []
        try:
            with open('Datasets/email-Eu-core-department-labels.txt') as fp:
                for line in fp.readlines():
                    node, label = line.strip().split(' ')
                    labels.append((int(node), int(label)))
        except IOError as err:
            raise Exception("Error loading labels data " + err)
        
        return labels

    def process(self):
        pass
    def load(self):
        features = 100
        graph = nx.Graph()
        edge_list = list()
        try:
            with open('Datasets/email-Eu-core.txt', encoding='utf-8') as fp:
                for line in fp.readlines():
                    source, target = line.split(' ')
                    edge_list.append((int(source), int(target)))
        except IOError as err:
            raise Exception('Error opening edges list' + err)

        graph.add_edges_from(edge_list)
        dimension = graph.number_of_nodes()
        print('Nodes:', graph.number_of_nodes())
        print('Edges:', graph.number_of_edges())

        adj = nx.to_scipy_sparse_array(graph, format='coo')
        
        
        index = torch.LongTensor(
            np.vstack((adj.row, adj.col))
        )
        
        data = Data(edge_index=torch.transpose(index, 0, 1))
        
        
        encoder = embedding.Encoder(data, index, device=self.device)

        x=None
        try:
            x = torch.load('Email_embedding.pt')
            if isinstance(x, Node2Vec):
                x = x()
        except Exception as err:
            emb = encoder.get_trained_model()
            x = emb
        torch.save(x, 'Email_embedding.pt')
        
        data.x = x
        
        data.y = [node_comm[1] for node_comm in self.loadLabels()]

        self.data = data
        

class Youtube(InMemoryDataset):
    def __init__(self, root='youtube_dataset', transform=None, pre_transform=None, device='cpu', config=''):
        super().__init__(root, transform, None, None)
        self.labels = []
        self.device=device
        self.name='Youtube'
        self.load()

    @property
    def raw_file_names(self):
        return ['edges.csv', 'group-edges.csv', 'groups.csv', 'nodes.csv']
    
    @property
    def processed_file_names(self):
        return ['youtube.pt']
    
    def download(self):
        pass

    def loadLabels(self):
        labels = []
        try:
            with open('Datasets/YouTube-dataset/groups.csv') as fp:
                for line in fp.readlines():
                    node, label = line.strip().split(',')
                    labels.append((int(node), int(label)))
        except IOError as err:
            raise Exception("Error loading labels data " + err)
        
        return labels

    def process(self):
        pass
    def load(self):
        features = 100
        graph = nx.Graph()
        edge_list = list()
        try:
            with open('Datasets/YouTube-dataset/edges.csv', encoding='utf-8') as fp:
                for line in fp.readlines():
                    source, target = line.split(',')
                    edge_list.append((int(source), int(target)))
                    #graph.add_edge(source, target)
        except IOError as err:
            raise Exception('Error opening edges list' + err)

        graph.add_edges_from(edge_list)
        dimension = graph.number_of_nodes()
        print('Nodes:', graph.number_of_nodes())
        print('Edges:', graph.number_of_edges())

        adj = nx.to_scipy_sparse_array(graph, format='coo')
        
        index = torch.LongTensor(
            np.vstack((adj.row, adj.col))
        )
        
        data = Data(edge_index=torch.transpose(index, 0, 1))
        encoder = embedding.Encoder(data, index, device=self.device)

        x=None
        try:
            x = torch.load('Youtube_embedding.pt')
            if isinstance(x, Node2Vec):
                x = x()
        except Exception as err:
            emb = encoder.get_trained_model()
            x = emb
        torch.save(x, 'Youtube_embedding.pt')
        
        data.x = x
        data.y = [node_comm[1] for node_comm in self.loadLabels()]

        self.data = data
        

class CellPhoneCalls(InMemoryDataset):
    def __init__(self, transform=None, device='cpu', config=''):
        #super(CellPhoneCalls, self).__init__('.', transform, None, None)
        raise NotImplementedError()
    
    def step(self):
        raise NotImplementedError()
class HospitalDataset(InMemoryDataset):
    def __init__(self, transform=None, device='cpu', config=''):
        super(HospitalDataset, self).__init__('.', transform, None, None)
        self.name='Hospital'
        G, comms = self.loadDatasource()

        self.data, self.slices = self.collate(graphToTensor(G, comms, device))
    
    def step(self):
        raise NotImplementedError()

    def loadDatasource(self):
        timeline = dict()
        with open('Datasets/detailed_list_of_contacts_Hospital.dat_') as fp:
            while True:
                line = fp.readline()
                if not line or line == '':
                    break
                line = line.strip()
                timestamp, source, target, s_role, t_role = [field for field in line.split('\t') if field != '']

                if timeline.get(timestamp, -1) == -1:
                    timeline[timestamp] = nx.Graph()
                timeline[timestamp].add_edge(source, target)

        graphs_view = sorted(timeline.keys())
        graph_iter = iter(graphs_view)        
        graph_0 = timeline.get(next(graph_iter))

        # Graph stats
        print(f'{len(timeline)} snapshots')
        print(f'Primeiro grafo com {graph_0.number_of_nodes()} vértices e {graph_0.number_of_edges()} arestas')
        
        return graph_0, None

                    


class SYNDataset(InMemoryDataset):
    ''' 
    @description: Dataset composed of 4 communities with 32 nodes each.
    '''
    def __init__(self, transform=None, device='cpu', config=''):
        super(SYNDataset, self).__init__('.', transform, None, None)
        self.name="SYN"
        self.device=device
        num_communities=4
        nodes_per_community=32
        dimension = num_communities * nodes_per_community
        p_in=0.5
        #p_out=0.1
        #p_out=0.15625      #Z=5
        p_out=0.1875        #Z=6
        z=16                # average degree
        self.entropy=0.3    # Wang usa 10% e 30% de entropia para cada passo
        rng = np.random.default_rng()

        # TODO: Verifica se existe um arquivo com o grafo criado, caso contrário
        # criar o grafo e separa as comunidades
        G = nx.Graph()
        # Create vertex
        for i in range(dimension):
            G.add_node(i)

        # Create edges
        for n in G.nodes:
            if G.number_of_edges()/G.number_of_nodes() >= z:
                break 
            for j in G.nodes:
                if n == j:
                    continue
                
                # Nos da mesma comunidade
                rand = rng.random()
                if (n % num_communities) == (j % num_communities):
                    if rand <= p_in:
                        G.add_edge(n, j)
                else:
                    if rand <= p_out:
                        G.add_edge(n, j)
        print(f'Grau do grafo: {G.number_of_edges()/G.number_of_nodes()}')
        
        # Create communities dict
        comms = dict()
        for i in range(num_communities):
            comms[i] = set()
        for n in G.nodes:
            comm_id = n % num_communities
            comms[comm_id].add(n)

        nx_comms = [k for k,v in comms.items() for z in v]

        self.data, self.slices = self.collate(graphToTensor(G, nx_comms, device))
        self.comms = comms
        self.G = G

    def step(self):        
        entropy_percentage=self.entropy
        comm_ids = set(self.comms.keys())
        for comm_id in self.comms.keys():
            entropy = int(entropy_percentage * len(self.comms[comm_id]))
            
            # ids do vértices que mudarão de comunidade
            ids = set(np.random.choice(list(self.comms[comm_id]), entropy))
            for _id in ids:
                # Gera a comunidade de destino
                comm_dest = np.random.choice(list(comm_ids - set([comm_id])))
                # Remove o id da comunidade de origem
                self.comms[comm_id].remove(_id)
                # Insere o id na comunidade de destino
                self.comms[comm_dest].add(_id)
        
        nx_comms = [k for k,v in self.comms.items() for z in v]
        self.data, self.slices = self.collate(graphToTensor(self.G, nx_comms, self.device))


            
class SYNFIXDataset(InMemoryDataset):
    def __init__(self, transform=None, device='cpu', config=''):
        super(SYNFIXDataset, self).__init__('.', transform, None, None)
        self.name="SYN-FIX"
        self.device=device
        num_communities=4
        nodes_per_community=32
        dimension = num_communities * nodes_per_community
        p_in=0.5
        #p_out=0.1
        p_out=0.09375       #Z=3
        #p_out=0.15625           #Z=5
        z=16 # average degree
        self.entropy=3
        rng = np.random.default_rng()

        # TODO: Verifica se existe um arquivo com o grafo criado, caso contrário
        # criar o grafo e separa as comunidades
        G = nx.Graph()
        for i in range(dimension):
            G.add_node(i)

        # Create edges
        #while (G.number_of_edges()/G.number_of_nodes()) < z:
        for n in G.nodes: 
            if G.number_of_edges()/G.number_of_nodes() >= z:
                break
            for j in G.nodes:
                #if G.degree[n] >= z:
                #    break
                if n == j:
                    continue
                #if G.degree[j] >= z:
                #    continue
                
                # Nos da mesma comunidade
                rand = rng.random()
                if (n % num_communities) == (j % num_communities):
                    if rand <= p_in:
                        G.add_edge(n, j)
                else:
                    if rand <= p_out:
                        G.add_edge(n, j)
        print(f'Grau do grafo: {G.number_of_edges()/G.number_of_nodes()}')
            
        # Create communities dict
        comms = dict()
        for i in range(num_communities):
            comms[i] = set()
        for n in G.nodes:
            comm_id = n % num_communities
            comms[comm_id].add(n)

        nx_comms = [k for k,v in comms.items() for z in v]

        self.data, self.slices = self.collate(graphToTensor(G, nx_comms, device))
        self.comms = comms
        self.G = G

    def step(self):
        
        comm_ids = set(self.comms.keys())
        for comm_id in self.comms.keys():
            # ids do vértices que mudarão de comunidade
            ids = set(np.random.choice(list(self.comms[comm_id]), self.entropy))
            for _id in ids:
                # Gera a comunidade de destino
                comm_dest = np.random.choice(list(comm_ids - set([comm_id])))
                # Remove o id da comunidade de origem
                self.comms[comm_id].remove(_id)
                # Insere o id na comunidade de destino
                self.comms[comm_dest].add(_id)
        
        nx_comms = [k for k,v in self.comms.items() for z in v]
        self.data, self.slices = self.collate(graphToTensor(self.G, nx_comms, self.device))


class SYNVARDataset(InMemoryDataset):
    def __init__(self, transform=None, device='cpu', config=''):
        super(SYNVARDataset, self).__init__('.', transform, None, None)
        self.name='SYN-VAR'
        self.timestamp=0
        self.device=device
        num_communities=4
        nodes_per_community=64
        dimension = num_communities * nodes_per_community
        p_in=0.5
        #p_out=0.1
        p_out=0.09375       #Z=3
        #p_out=0.15625           #Z=5
        z=16 # average degree
        self.rng = np.random.default_rng()

        # TODO: Verifica se existe um arquivo com o grafo criado, caso contrário
        # criar o grafo e separa as comunidades
        G = nx.Graph()
        for i in range(dimension):
            G.add_node(i)

        # Create edges
        for n in G.nodes: 
            for j in G.nodes:
                if G.degree[n] >= z:
                    break
                if n == j:
                    continue
                if G.degree[j] >= z:
                    continue
                
                # Nos da mesma comunidade
                rand = self.rng.random()
                if (n % num_communities) == (j % num_communities):
                    if rand <= p_in:
                        G.add_edge(n, j)
                else:
                    if rand <= p_out:
                        G.add_edge(n, j)
        
        # Create communities dict
        comms = dict()
        for i in range(num_communities):
            comms[i] = set()
        for n in G.nodes:
            comm_id = n % num_communities
            comms[comm_id].add(n)

        nx_comms = [k for k,v in comms.items() for z in v]

        self.data, self.slices = self.collate(graphToTensor(G, nx_comms, device))
        self.comms = comms
        self.G = G
        self.community_journal = dict()

    def step(self):
        if self.timestamp <=2 and self.timestamp <= 5:
            # Take 8 nodes for each comm and create a new one with this nodes
            # 5 timestamps later those nodes will return to previous comm
            new_comm = max(self.comms.keys()) + 1
            for comm_id in self.comms.keys():
                nodes=[(comm_id, node) for node in self.rng.choice(self.comms[comm_id],size=8)]
                self.comms[new_comm].update(nodes)
                [self.comms[comm_id].remove(x) for x in nodes]
            self.community_journal[new_comm] = {
                'timestamp': self.timestamp,
                'nodes': nodes
                }
        else:
            # take nodes back to original communities
            for comm_id,comm_info in self.community_journal.items():
                if (comm_info['timestamp']+5) == self.timestamp:
                    for old_comm, node in comm_info['nodes']:
                        self.comms[old_comm].add(node)
                    del self.community_journal[comm_id]

        if self.timestamp <=2 and self.timestamp <= 10:
            # delete and create nodes
            pass
        self.timestamp +=1

class SYNEVENTDataset(InMemoryDataset):
    def __init__(self, transform=None, device='cpu', config=''):
        super(SYNEVENTDataset, self).__init__('.', transform, None, None)

    def step(self):
        raise NotImplementedError()
class DynLRFDataset(InMemoryDataset):
    def __init__(self, transform=None, device='cpu', config=''):
        super(DynLRFDataset, self).__init__('.', transform, None, None)
        self.name = 'SYN-Event*'
        self.current_step = 0
        self.device = device
        #strategy = 'StdMerge'
        #strategy = 'StdGrow'
        strategy = 'StdMixed'
        p_in=.5
        p_out=0.09375       #Z=3
        #p_out=0.15625           #Z=5
        #num_communities = 4
        #nodes_per_community = 32
        dimension = 500

        # This module coms from https://github.com/rkdarst/dynbench
        sys.path.append('/home/aurelio/devel/python/dynbench')
        import bm

        #self.bm_model = bm.get_model(name=strategy, p_in=.5, p_out=.1, n=nodes_per_community, q=num_communities)
        self.bm_model = bm.get_model(name=strategy, p_in=p_in, p_out='k=3', n=dimension)

        G = self.bm_model.graph(self.current_step)        
        comms = self.bm_model.comms(self.current_step)

        root_logger.info(f"Network model created with {G.number_of_nodes()} nodes and {G.number_of_edges()} edges")
        nx_comms = [k for k,v in comms.items() for z in v]

        self.data, self.slices = self.collate(graphToTensor(G, nx_comms, device))

        root_logger.info(f'Grau do grafo: {G.number_of_edges()/G.number_of_nodes()}')

    def step(self):
        self.current_step += 1

        G = self.bm_model.graph(self.current_step)        
        comms = self.bm_model.comms(self.current_step)

        #root_logger.info(f"Network model created with {G.number_of_nodes()} nodes and {G.number_of_edges()} edges")
        nx_comms = [k for k,v in comms.items() for z in v]

        self.data, self.slices = self.collate(graphToTensor(G, nx_comms, self.device))

                
#normalize = lambda x: 1/x if x != 0 else 0.
def normalize(x):
    return 1/x if x != 0 else 0.

def graphToTensor(graph, comms, device='cpu'):
    labels = np.asarray(comms).astype(np.int64)
    dimension = graph.number_of_nodes()
        
    adj = nx.to_scipy_sparse_array(graph).tocoo()
    row = torch.from_numpy(adj.row.astype(np.int64)).to(torch.long)
    col = torch.from_numpy(adj.col.astype(np.int64)).to(torch.long)
    edge_index = torch.stack([row,col], dim=0)

    sp=nx.shortest_path_length(graph)
    array = list()
    for i in sp:
        d=i[1]
        row = list()
        d_sorted = dict(sorted(d.items()))
        for key in d_sorted.keys():
            row.append(normalize(d_sorted[key]))
        array.append(row)
    embeddings = np.asarray(array, dtype=float)

    scale = StandardScaler()
    embeddings = scale.fit_transform(embeddings.reshape(dimension,dimension))


    data = Data(edge_index=edge_index)
    data.num_nodes = graph.number_of_nodes()
    data.x = torch.from_numpy(embeddings).type(torch.float32)

    # Lift the tensor do eliminate negative values
    data.x = data.x - np.min(data.x.numpy())

    data.x.to(device)

    y = torch.from_numpy(labels).type(torch.long).to(device)
    #y = F.one_hot(y, num_classes=num_communities)
    data.y = y.clone().detach()

    # splitting the data into train, validation and test
    X_train, X_test, y_train, y_test = train_test_split(pd.Series(graph.nodes()), 
                                                        pd.Series(labels),
                                                        test_size=0.30, 
                                                        random_state=42)
    
    n_nodes = graph.number_of_nodes()
    
    # create train and test masks for data
    train_mask = torch.zeros(n_nodes, dtype=torch.bool).to(device)
    test_mask = torch.zeros(n_nodes, dtype=torch.bool).to(device)
    train_mask[X_train.index] = True
    test_mask[X_test.index] = True
    data['train_mask'] = train_mask
    data['test_mask'] = test_mask

    return [data]
    #data, slices = InMemoryDataset().collate([data])
    #return data, slices


def load_graph_data(config: dict, device):
    communities, graph = load_dataset("data/data3.t00100.comms", "data/data3.t00100.graph")

    data_graph = from_networkx(graph)
    data_graph['x'] = torch.tensor([[int(x)] for x in communities.keys()], dtype=torch.float, device=device)

    node_features = data_graph['x']
    print("Features shape", node_features.shape)
    #shape(E,2)
    topology = data_graph['edge_index']
    print("Topology shape", topology.shape)
    #shape(N,1)
    node_labels = torch.tensor([int(label) for label in communities.values()], dtype=torch.long, device=device)
    print("Labels shape", node_labels.shape)


    num_nodes = graph.number_of_nodes()

    train_indices=torch.arange(0, 0.2*num_nodes, dtype=torch.long, device=device)
    val_indices=torch.arange(0.2*num_nodes, 0.6*num_nodes, dtype=torch.long, device=device)
    test_indices=torch.arange(0.6*num_nodes, 0.9*num_nodes, dtype=torch.long, device=device)

    return node_features, node_labels, topology, train_indices, val_indices, test_indices


def load_dataset(comms_file: str, graph_file: str) -> tuple:
    '''
        return the communities dict and the graph
    '''

    comms = parse_community_file(comms_file)
    G = nx.read_weighted_edgelist(graph_file)

    return comms, G

def parse_community_file(comms_file: str) -> dict:
    '''
        return a dict with node id as key and its assoated community
        TODO: This method only covers disjoint communities YET !
    '''
    nodes = dict()

    with open(comms_file, 'rt') as fp:
        while True:
            line = fp.readline()
            if not line:
                break
            line = line.strip()
            if line[0] == "#" or line == '':
                continue
            node_id,cmty_id = line.split(' ')
            nodes[node_id]=cmty_id
            
    return nodes

def transform_community_assignment(comm_nodes):
    flat_comms = list()
    
    for comm in comm_nodes.keys():
        for node in comm_nodes[comm]:
            flat_comms.insert(node, comm)

    return flat_comms
    

def savemat(ds: InMemoryDataset):
    from scipy.io import savemat

    for i in range(20):
        matrix = nx.convert_matrix.to_numpy_matrix(ds.G)        
        savemat(f'contenders/GenLouvain-2.2.0/synfix{i}.mat', {'adj': matrix})
        
        flat_comms = transform_community_assignment(ds.comms)
        savemat(f'contenders/GenLouvain-2.2.0/synfix{i}.comms', {'comm': np.array(flat_comms)})
            
        print(f'Nodes: {ds.G.number_of_nodes()}\nEdges: {ds.G.number_of_edges()}\nAvg. degree: {ds.G.number_of_edges()/ds.G.number_of_nodes()}')
        #print(ds.comms)
        ds.step()


if __name__ == "__main__":
    print('Loading environments')
    
    #ds = SYNDataset()
    #ds=SYNFIXDataset()
    #ds = HospitalDataset()
    #savemat()
    
    ds = Email_Eu_core('data/email-Eu')
    

