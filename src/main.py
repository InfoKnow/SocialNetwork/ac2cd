import cdgat
import dataset
import networkx as nx
import time
import os
from datetime import datetime
import torch
from torch.optim import Adam, ASGD
import matplotlib.pyplot as plt
from dataclasses import dataclass
from sklearn.metrics import normalized_mutual_info_score, f1_score
import numpy as np

@dataclass()
class Config:
    max_epochs: int = 20000
    validation_interval: int = 50
    checkpoint_interval = 5000
    patience_threshold = 200
    timespan: int = 100
    device: str = 'cpu'


start_time = time.time()

#torch.backends.cudnn.deterministic = True
#torch.backends.cudnn.benchmark = False
#torch.use_deterministic_algorithms(True)
#torch.manual_seed(0)
#torch.cuda.manual_seed(0)
#torch.cuda.manual_seed_all(0)
#np.random.seed(0)

# Hyperparameters
config = Config()

config.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#device = 'cpu'

#DS = dataset.DynLRFDataset(device=config.device)
DS = dataset.SYNDataset(device=config.device)
#DS = dataset.SYNFIXDataset(device=config.device)
#DS = dataset.SYNVARDataset(device=config.device)
data = DS[0]
data = data.to(config.device)

model = cdgat.get_model(DS.num_features, DS.num_classes, config.device)
#model.apply(cdgat.GATNet.init_weights)
optimizer = Adam(model.parameters(), lr=1e-3, eps=1e-8, weight_decay=5e-4)
#optimizer = ASGD(model.parameters(), lr=1e-3, alpha=.5)

print("Training model")
loss_values = []
nmi_values = []
test_acc_values = []
train_acc_values = []
epochs = []
patience_count = 0
BEST_ACC=0
current_run_path = f'runs/{datetime.now().strftime("%Y%m%d%H%M%S")}'
os.makedirs(current_run_path, exist_ok=True)
for epoch in range(config.max_epochs):
    current_loss = cdgat.train_model(model, data, optimizer)
    loss_values.append(current_loss)
    if (epoch + 1) % config.validation_interval == 0:
    
        train_acc, test_acc, nmi = cdgat.test(model, data)
        test_acc_values.append(test_acc)
        train_acc_values.append(train_acc)
        nmi_values.append(nmi)
        epochs.append(epoch+1)
        if test_acc > BEST_ACC:
            BEST_ACC = test_acc
            patience_count = 0
        else:
            patience_count += 1

        print(f'Epoch: {epoch+1:03d}, Train: {train_acc:.4f}, '
            f'Test: {test_acc:.4f}',
            f'NMI: {nmi:.4f}',
            f'time: {(time.time() - start_time):.4f} seconds'
            )
    if (epoch + 1) % config.checkpoint_interval == 0:
        print("Stalled !!", "Checkpoint saved")
        cdgat.save_checkpoint(model, optimizer, f'{current_run_path}/cdgat_epoch-{epoch}.pth', epoch) 
    if patience_count > config.patience_threshold:
        print("Stalled !!", "Finish training")
        break


print("Model trained,","testing")
X = list()
Y = list()
f1_micro = list()
f1_macro = list()
with torch.no_grad():
    for i in range(1,21):
        DS.step()
        data = DS[0]
        data = data.to(config.device)
        predicted = model(data.x, data.edge_index)
        out_cpu = torch.argmax(predicted.detach().cpu(), dim=-1)
        y_cpu = data.y.detach().cpu()
        nmi = normalized_mutual_info_score(y_cpu, out_cpu, average_method='arithmetic')
        f1_micro.append(
            f1_score(y_cpu, out_cpu, average='micro')
        )
        f1_macro.append(
            f1_score(y_cpu, out_cpu, average='macro')
        )
        X.append(i)
        Y.append(nmi)

fig = plt.figure()
ax1 = fig.add_subplot(211)
fig.suptitle(DS.name)
ax1.set_ylabel('NMI')
ax1.set_xlabel('Time step')
ax1.plot(X,Y, label='NMI Score')
ax1.plot(X,f1_micro, label='Micro-F1')
ax1.plot(X,f1_macro, label='Macro-F1')
plt.xticks(np.arange(0, 21, 5.0))

plt.legend()
#plt.show()
plt.savefig("resultado.png")
plt.close()

# Print loss graph
fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.set_ylabel('Loss value')
ax1.set_xlabel('Epochs')
ax1.plot(loss_values)
plt.savefig("loss.png")
