import torch

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


model = cdgat.get_model(DS.num_features, DS.num_classes, device)
optimizer = Adam(model.parameters(), lr=0.01, eps=1e-8, weight_decay=5e-4)