from datetime import datetime
import os
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.distributions.categorical import Categorical

from torch.utils.tensorboard import SummaryWriter

import cdgat


class PPOMemory:
    '''
    Episode memory
    '''
    def __init__(self, batch_size):
        self.states = []
        self.probs = []
        self.vals = []
        self.actions = []
        self.rewards = []
        self.dones = []

        self.batch_size = batch_size

    def generate_batches(self):
        '''
        Generate a batch of episodes
        '''
        n_states = len(self.states)
        batch_start = np.arange(0, n_states, self.batch_size)
        indices = np.arange(n_states, dtype=np.int64)
        np.random.shuffle(indices)
        batches = [indices[i:i+self.batch_size] for i in batch_start]

        return  np.array(self.states), \
                np.array(self.actions), \
                np.array(self.probs), \
                np.array(self.vals), \
                np.array(self.rewards), \
                np.array(self.dones), \
                batches

    def store_memory(self, state, action, probs, vals, reward, done):
        '''
        Stores a tuple of steps information
        '''
        self.states.append(state)
        self.actions.append(action)
        self.probs.append(probs)
        self.vals.append(vals)
        self.rewards.append(reward)
        self.dones.append(done)

    def clear_memory(self):
        '''
        Clear the memory, wiping all internal vectors
        '''
        self.states = []
        self.probs = []
        self.vals = []
        self.actions = []
        self.rewards = []
        self.dones = []


class ActorNetwork(nn.Module):
    '''
    Actor network, wrapper class for the actual gnn
    '''
    def __init__(self, n_actions, input_dims, alpha, chkpt_dir='tmp/ppo', device='cpu', nn_type='gat'):
        super(ActorNetwork, self).__init__()

        self.device=device
        self.checkpoint_file = os.path.join(chkpt_dir, f'actor_torch_ppo-{nn_type}.pkl')
        if nn_type == 'gat':
            self.actor = cdgat.GATNet(input_dims[1], n_actions)
        else:
            self.actor = cdgat.GCNNet(input_dims[1], n_actions)

        #self.optimizer = optim.Adam(self.parameters(), lr=alpha)
        self.optimizer = optim.SGD(self.parameters(), lr=alpha)
        self.to(self.device)

    def forward(self, state, edge_index):
        '''
        Forward method of the nn module
        '''
        x = None
        if isinstance(state, torch.Tensor):
            x=state.clone().detach().to(self.device)
        else:
            x=torch.from_numpy(state).to(self.device)

        dist = self.actor(
            x=x,
            edge_index=edge_index.to(self.device)
            )

        return Categorical(dist)

    def save_checkpoint(self):
        '''
        Save the current state of the nn
        '''
        torch.save(self.state_dict(), self.checkpoint_file)
    def load_checkpoint(self):
        '''
        Load the state from a previously saved state
        '''
        self.load_state_dict(torch.load(self.checkpoint_file))

class CriticNetwork(nn.Module):
    '''
    Critic network, wrapper class for the actual gnn
    '''
    def __init__(self, input_dims, alpha, chkpt_dir='tmp/ppo', device='cpu', nn_type='gat'):
        super(CriticNetwork, self).__init__()

        self.checkpoint_file = os.path.join(chkpt_dir, f'critic_torch_ppo-{nn_type}.pkl')

        if nn_type == 'gat':
            self.critic = cdgat.GATNet(input_dims[1], 1, dropout=0.6, critic=True)
        else:
            self.critic = cdgat.GCNNet(input_dims[1], 1)

        #self.optimizer = optim.Adam(self.parameters(), lr=alpha)
        self.optimizer = optim.SGD(self.parameters(), lr=alpha)
        self.device = device
        self.to(self.device)

    def forward(self, state, edge_index):
        '''
        Forward method of the nn module
        '''
        state_tensor = None
        if isinstance(state, torch.Tensor):
            state_tensor=state.clone().detach().to(self.device)
        else:
            state_tensor=torch.from_numpy(state).to(self.device)
        ret=self.critic(x=state_tensor, edge_index=edge_index.to(self.device))

        return ret

    def save_checkpoint(self):
        '''
        Save the current state of the nn
        '''
        torch.save(self.state_dict(), self.checkpoint_file)
    def load_checkpoint(self):
        '''
        Load the state from a previously saved state
        '''
        self.load_state_dict(torch.load(self.checkpoint_file))

class Agent:
    '''
    The agent that interacts with the environment
    '''
    def __init__(
            self,
            n_actions,
            input_dims,
            gamma=0.99,
            alpha=0.0003,
            gae_lambda=0.99,
            policy_clip=0.4,
            batch_size=64,
            N=2048,
            n_epochs=10,
            device='cpu',
            nn_type='gat',
            writer=None
        ):
        self.gamma = gamma
        self.policy_clip = policy_clip
        self.n_epochs = n_epochs
        self.gae_lambda = gae_lambda
        self.nn_type = nn_type
        self.device=device

        if writer is None:
            now = datetime.now()
            daterun = now.strftime('%Y%m%d%H%M%S')
            logdir=f'ppo_tensorboard-{daterun}_{self.nn_type}'
            self.writer = SummaryWriter(logdir)
        else:
            self.writer=writer


        self.actor = ActorNetwork(n_actions[0], input_dims, alpha, device=device, nn_type=nn_type)
        self.critic = CriticNetwork(input_dims, alpha, device=device, nn_type=nn_type)
        self.memory = PPOMemory(batch_size)

    def remember(self, state, action, probs, vals, reward, done):
        '''
        Stores the entire step into the memory
        '''
        self.memory.store_memory(state, action, probs, vals, reward, done)

    def save_models(self):
        '''
        Save actor and critic models for future loading
        '''
        print('... saving models ...')
        self.actor.save_checkpoint()
        self.critic.save_checkpoint()

    def load_models(self):
        '''
        Load both models
        '''
        print('... loading models ...')
        self.actor.load_checkpoint()
        self.critic.load_checkpoint()

    def choose_action(self, state, edge_index):
        '''
        Returns the action with higher probability
        '''
        torch.cuda.empty_cache()
        dist = self.actor(state, edge_index)
        
        value = self.critic(state, edge_index)
        action = dist.sample()

        probs = torch.squeeze(dist.log_prob(action)).cpu().detach().numpy()
        action = torch.squeeze(action).cpu().detach().numpy()
        #TODO: Conferir se esse tipo de agregação está OK
        value = torch.mean(torch.squeeze(value).cpu().detach())#.numpy()

        return action, probs, value
        
    def learn(self, edge_index):
        '''
        Training function
        '''
        self.critic.train()
        self.actor.train()
       
        for i in range(self.n_epochs):
            if self.device.find('cuda') > -1:
                torch.cuda.empty_cache()
            rng = np.random.default_rng()
            states, action_arr, old_probs_arr, vals_arr, reward_arr, dones_arr, batches = self.memory.generate_batches()

            values = vals_arr
            advantage = np.zeros(len(reward_arr), dtype=np.float32)

            for t in range(len(reward_arr)-1):
                discount = 1
                a_t = 0
                for k in range(t, len(reward_arr)-1):
                    a_t += discount*(reward_arr[k] + self.gamma*values[k+1]*(1-int(dones_arr[k])) - values[k])
                    discount *= self.gamma*self.gae_lambda
                advantage[t] = a_t # FIXME: confere isso
            advantage = torch.tensor(advantage).to(self.actor.device)


            values = torch.tensor(values).to(self.actor.device)
            for batch in batches:
                old_probs = torch.tensor(old_probs_arr[batch]).to(self.actor.device)
                actions = torch.tensor(action_arr[batch]).to(self.actor.device)

                dist = self.actor(rng.choice(states[batch], size=1)[0], edge_index)
                critic_value = self.critic(rng.choice(states[batch], size=1)[0], edge_index)
                critic_value = torch.squeeze(critic_value)

                new_probs = dist.log_prob(actions)
                prob_ratio = new_probs.exp() / old_probs.exp()
                weighted_probs = advantage[batch] * torch.t(prob_ratio)
                weighted_clipped_probs = torch.t(torch.clamp(prob_ratio, 1-self.policy_clip, 1+self.policy_clip))*advantage[batch]
                actor_loss=-torch.min(weighted_probs, weighted_clipped_probs).mean()

                returns = advantage[batch] + values[batch]

                # TD Error
                #FIXME: precisa verificar esse batch do critic_value
                critic_loss = (returns - critic_value[batch])**2
                critic_loss = critic_loss.mean()

                #print("Critic loss", critic_loss.item(), "Actor loss", actor_loss.item())

                self.writer.add_scalar('actor_loss', actor_loss.item(), i)
                self.writer.add_scalar('critic_loss', critic_loss.item(), i)

                total_loss = actor_loss.item() + 0.5*critic_loss.item()

                self.writer.add_scalar('total_loss', total_loss, i)
                #writer.add_scalar('reward', reward)

                self.actor.optimizer.zero_grad(set_to_none=True)
                self.critic.optimizer.zero_grad(set_to_none=True)
                actor_loss.backward()
                ciritic_loss.backward()
                #total_loss.backward()
                self.actor.optimizer.step()
                self.critic.optimizer.step()
        self.writer.flush()

        self.memory.clear_memory()