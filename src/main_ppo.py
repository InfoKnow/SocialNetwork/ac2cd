import time
import logging

from dataclasses import dataclass
import numpy as np
from PPO2CD import Agent
from utils import plot_learning_curve, timer_func
from environment import GATEnv
import dataset
from pydantic import BaseSettings

from torch_geometric.datasets import AttributedGraphDataset
from torch_geometric.data import Dataset

from datetime import datetime

now=datetime.now()

daterun=now.strftime('%Y%m%d%H%M%S')

from torch.utils.tensorboard import SummaryWriter

# Inspired by
# https://github.com/arcosta/Youtube-Code-Repository/tree/master/ReinforcementLearning/PolicyGradient/PPO/torch


class Settings(BaseSettings):
    '''
    @description: Hyperparameters default configuration
    '''
    max_epochs: int = 20000
    validation_interval: int = 50
    checkpoint_interval = 5000
    patience_threshold = 100
    timespan: int = 100
    device: str = 'cuda'
    nn_type: str = 'gat'
    learn_rate: int = 50
    batch_size: int = 20
    n_epochs: int = 2000
    alpha: float = 3e-04
    n_games: int = 100
    figure_file: str = 'plots/AC2CD.png'
    class Config:
        env_file: str = '.env'
        env_file_encoding: str = 'utf-8'

FORMAT = '%(asctime)-15s %(process)d %(name)-8s %(message)s'
logging.basicConfig(format=FORMAT)

start_time = time.time()

if __name__ == '__main__':
    config = Settings()
    DS: Dataset #= dataset.Email_Eu_core(device=config.device)
    DS = dataset.High_School(device=config.device)
    #DS = dataset.BlogCatalog(device=config.device)
    env: GATEnv = GATEnv(DS)

    logdir = f'ppo_tensorboard-{daterun}_{config.nn_type}_{DS.name}'
    writer = SummaryWriter(logdir)

    agent = Agent(
            n_actions=env.action_space.shape,
            input_dims=env.observation_space.shape,
            batch_size=config.batch_size,
            alpha=config.alpha,
            n_epochs=config.n_epochs,
            device=config.device,
            nn_type=config.nn_type,
            writer=writer
        )
    best_score = env.reward_range[0]
    score_history = []

    learn_iters: int = 0
    avg_score: float = 0.0
    n_steps: int = 0

   

    #
    # Agent pre-train
    #
    


    # Main loop
    for i in range(config.n_games):
        observation = env.reset()
        done: bool = False
        score: float = 0.0
        while not done:
            action, prob, val = agent.choose_action(observation, env.edge_index())
            observation_, reward, done, info = env.step(action.tolist())
            env.render()
            n_steps += 1
            score += reward
            agent.remember(
                    observation.cpu().numpy(),
                    action.squeeze(),
                    prob.squeeze(),
                    val.squeeze(),
                    reward,
                    done
                )

            writer.add_scalar('value', val.item(), i)
            writer.add_scalar('reward', reward, i)

            if n_steps % config.learn_rate == 0:
                logging.info(f'Going for training at step {n_steps}')
                agent.learn(env.edge_index())
                learn_iters += 1
            agent.actor.eval()
            agent.critic.eval()
            observation = observation_
        score_history.append(score)
        avg_score = np.mean(score_history[-100:])

        if avg_score > best_score:
            best_score = avg_score
            agent.save_models()

        print(
                f'episode {i}',
                f'score {score:0.1f}',
                f'avg score {avg_score:0.1f}',
                f'time_steps {n_steps}',
                f'learning_steps {learn_iters}'
            )
