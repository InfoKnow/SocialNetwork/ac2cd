import igraph as ig

#communities, graphdata = load_dataset("data/data3.t00100.comms", "data/data3.t00100.graph")

# Nodes is the dict assinging nodes to communities
nodes = dict()

with open(comms_file, 'rt') as fp:
    while True:
        line = fp.readline()
        if not line:
            break
        line = line.strip()
        if line[0] == "#" or line == '':
            continue
        node_id,cmty_id = line.split(' ')
        nodes[node_id]=cmty_id


G = ig.Graph.Read_Edgelist("data/data3.t00100.graph")
 

print(f"Grafo carregado com {len(G.vs)} vértices")
