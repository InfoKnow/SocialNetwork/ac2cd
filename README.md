## Actor-Critic to Community Detection (AC2CD)
AC2CD is  a reinforcement learning-based framework to community detection in online social networks, using the Actor-Critic approach and the PPO strategy.
The implementation uses GAT-based neural networks in both actor and critic agents.

This repo implements the architecture defined in the paper 
> Costa, Aurélio Ribeiro, and Célia Ghedini Ralha. "AC2CD: An actor–critic architecture for community detection in dynamic social networks." Knowledge-Based Systems 261 (2023): 110202.



![Architecture implemented](https://gitlab.com/InfoKnow/SocialNetwork/ac2cd/-/raw/main/img/Arquitetura-AC2CD-GeneralArch.png)

# Running the experiment
To install all necessarty dependencies just run ```pip install -r requirements.txt``` on the root directory.
We recommend the use of some virtual environment like pyenv to manage your libraries.

To run the reinforcement learning implementation, type ```python main_ppo.py```
The configuration of the implementation is guided by a .env file with the non-default parameters.

This is a work in progress and we are currently devoting efforts to improve the performance of the framework.

