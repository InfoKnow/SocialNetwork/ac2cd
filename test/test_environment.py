import unittest
import os
import sys
import torch
import numpy as np
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


from src import environment
from src import dataset
from src.utils import timer_func

class TestEnv(unittest.TestCase):
    def setUp(self):
        self.dataset = dataset.SYNDataset(device='cpu')

    def test_modularity_density(self):
        comms = [
            [1,2,3],
            [4,5,6],
        ]
        
        edge_index = [
            (1,2),
            (1,3),
            (2,3),
            (3,4),
            (4,5),
            (4,6),
            (5,6)
        ]

        if self.dataset is not None:
            data = list(self.dataset.comms.values())
            comms = np.array(
                    [list(comm) for comm in data], 
                    dtype=np.float32
                )
            edge_index = self.dataset.G.edges()
            
        print("Communities len", comms.shape)
        print("Num edges", len(edge_index))
        wrapper_func = timer_func(environment.GATEnv.modularity_density)
        qds = wrapper_func(torch.tensor(comms), edge_index)

        print("Modularity density",qds)
        assert int(qds) == 3, f'Modularity density with score: {qds}'

    def testx_mod_density(self):

        comms = [
            [1,2,3],
            [4,5,6],
        ]
        
        edge_index = [
            (1,2),
            (1,3),
            (2,3),
            (3,4),
            (4,5),
            (4,6),
            (5,6)
        ]

        qds = environment.GATEnv.mod_density(torch.tensor(comms), edge_index)

        print("Modularity density",qds)
        assert int(qds) == 3, f'Modularity density with score: {qds}'



if __name__ == "__main__":
    #unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEnv)
    unittest.TextTestRunner(verbosity=0).run(suite)